/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 20:45:18 by antgalan          #+#    #+#             */
/*   Updated: 2022/10/20 09:25:50 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief	Muestra por pantalla una combinacion.
 * 
 * @param comb	Puntero a la combinacion a mostrar.
 */
void	print_comb(char *comb)
{
	write(1, &comb[0], 1);
	write(1, &comb[1], 1);
	write(1, &comb[2], 1);
	if (!(comb[0] == '7' && comb[1] == '8' && comb[2] == '9'))
		write(1, ", ", 2);
}

void	ft_print_comb(void)
{
	char	comb[3];

	comb[0] = '0';
	while (comb[0] <= '7')
	{
		comb[1] = comb[0] + 1;
		while (comb[1] <= '8')
		{
			comb[2] = comb[1] + 1;
			while (comb[2] <= '9')
			{
				print_comb(comb);
				comb[2]++;
			}
			comb[1]++;
		}
		comb[0]++;
	}
}
