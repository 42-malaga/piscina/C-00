/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/20 09:21:02 by antgalan          #+#    #+#             */
/*   Updated: 2022/10/20 09:33:00 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief 	Muestra por pantalla una pareja de combinaciones.
 * 
 * @param a		Primera combinacion de la pareja.
 * @param b		Segunda combinacion de la pareja.
 */
void	print_pair(int a, int b)
{
	char	pair[5];

	pair[0] = '0' + a / 10;
	pair[1] = '0' + a % 10;
	pair[2] = ' ';
	pair[3] = '0' + b / 10;
	pair[4] = '0' + b % 10;
	write(1, pair, 5);
	if (!(a == 98 && b == 99))
		write(1, ", ", 2);
}

void	ft_print_comb2(void)
{
	int	a;
	int	b;

	a = 0;
	while (a <= 98)
	{
		b = a + 1;
		while (b <= 99)
		{
			print_pair(a, b);
			b++;
		}
		a++;
	}
}
