/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/20 09:38:20 by antgalan          #+#    #+#             */
/*   Updated: 2022/10/20 14:30:44 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief 	Muestra por pantalla una combinacion..
 * 
 * @param comb	Combinacion a mostrar.
 * @param size	Longitud de la combinacion.
 */
void	print_comb(int *comb, int size)
{
	char	c;
	int		i;

	i = 0;
	while (i < size)
	{
		c = '0' + comb[i];
		write(1, &c, 1);
		i++;
	}
	if (comb[0] < 10 - size)
		write(1, ", ", 2);
}

/**
 * @brief	Calcula la siguiente combinacion.
 * 
 * @param comb	Combinacion a mostrar.
 * @param size	Longitud de la combinacion.
 */
void	next_comb(int *comb, int size)
{
	int	max;
	int	i;

	i = size - 1;
	max = 9;
	while (comb[i] == max)
	{
		i--;
		max--;
	}
	comb[i]++;
	while (i < size)
	{
		comb[i + 1] = comb[i] + 1;
		i++;
	}
}

void	ft_print_combn(int nb)
{
	int	comb[10];
	int	i;

	i = 0;
	while (i < nb)
	{
		comb[i] = i;
		i++;
	}
	print_comb(comb, nb);
	while (comb[0] != 10 - nb || comb[nb - 1] != 9)
	{
		if (comb[nb - 1] != 9)
			comb[nb - 1]++;
		else
			next_comb(comb, nb);
		print_comb(comb, nb);
	}
}
